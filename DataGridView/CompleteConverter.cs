﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DataGridView
{
    public class CompleteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // โค้ดสำหรับแปลงค่า Complete ให้เป็นข้อความหรือรูปแบบอื่น ๆ ตามที่ต้องการ
            // ตัวอย่าง:
            bool complete = (bool)value;
            if (complete)
            {
                return "Completed";
            }
            else
            {
                return "Incomplete";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
